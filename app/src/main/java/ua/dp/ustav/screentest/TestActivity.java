package ua.dp.ustav.screentest;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ua.dp.ustav.screentest.adapter.TestsFragmentAdapter;

public class TestActivity extends AppCompatActivity {
    public static final String KEY_SECTION_NAME = "KEY_SECTION_NAME";
    public static final String KEY_ITEM_POSITION = "KEY_ITEM_POSITION";

    private TestsFragmentAdapter adapter;

    private ViewPager testsStrip;

    private TextView title;
    private TextView description;
    private TextView hint;
    private LinearLayout testDescription;

    private GestureDetector gestureDetector;

    private GestureDetector.OnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (Math.abs(velocityY) > Math.abs(velocityX)) {
                if (velocityY > 0) {
                    testDescription.setVisibility(View.GONE);
                    hideControls();
                } else {
                    testDescription.setVisibility(View.VISIBLE);
                }
            }

            return false;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            int visibility = testDescription.getVisibility();
            testDescription.setVisibility(visibility == View.VISIBLE ? View.GONE : View.VISIBLE);
            return false;
//            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            finish();
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tests_strip);
        testsStrip = (ViewPager) findViewById(R.id.tests_strip);

        String sectionName = getIntent().getStringExtra(KEY_SECTION_NAME);
        adapter = new TestsFragmentAdapter(getSupportFragmentManager(), this,  sectionName);

        testsStrip.setAdapter(adapter);

        gestureDetector = new GestureDetector(this, gestureListener);
        testsStrip.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return false;
            }
        });

        testsStrip.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {}

            @Override
            public void onPageSelected(int i) {
                setupTest(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {}
        });

        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        hint = (TextView) findViewById(R.id.hint);
        testDescription = (LinearLayout) findViewById(R.id.test_description);

        hideControls();

        int itemPosition = getIntent().getIntExtra(KEY_ITEM_POSITION, 0);
        if (itemPosition == 0) {
            setupTest(0);
        } else {
            testsStrip.setCurrentItem(itemPosition, false);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void hideControls() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private void setupTest(int position) {
        Test test = adapter.getTest(position);

        title.setText(test.getTitle());

        if (TextUtils.isEmpty(test.getDescription())) {
            description.setVisibility(View.GONE);
        } else {
            description.setVisibility(View.VISIBLE);
            description.setText(test.getDescription());
        }

        if (TextUtils.isEmpty(test.getHint())) {
            hint.setVisibility(View.GONE);
        } else {
            hint.setVisibility(View.VISIBLE);
            hint.setText(test.getHint());
        }

        if (description.getVisibility() == View.GONE && hint.getVisibility() == View.GONE) {
            testDescription.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                testsStrip.setCurrentItem(testsStrip.getCurrentItem() - 1, false);
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                testsStrip.setCurrentItem(testsStrip.getCurrentItem() + 1, false);
                return true;
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_SPACE:
                if (testDescription.getVisibility() == View.GONE) {
                    testDescription.setVisibility(View.VISIBLE);
                } else {
                    testDescription.setVisibility(View.GONE);
                    hideControls();
                }
                return true;
            case KeyEvent.KEYCODE_DPAD_UP:
                    testDescription.setVisibility(View.VISIBLE);
                return true;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                    testDescription.setVisibility(View.GONE);
                    hideControls();
                return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
