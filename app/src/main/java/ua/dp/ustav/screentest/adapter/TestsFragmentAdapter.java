package ua.dp.ustav.screentest.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ua.dp.ustav.screentest.Test;
import ua.dp.ustav.screentest.TestFragment;

public class TestsFragmentAdapter extends FragmentPagerAdapter {
    private Test[] mTests;

    public TestsFragmentAdapter(FragmentManager fm, Context context, String sectionName) {
        super(fm);

        int[] testIds;
        String[] testTitles;
        String[] testDescriptions;
        String[] testHints;

        Resources resources = context.getResources();
        String packageName = context.getPackageName();

        int itemIdsArrayId = resources.getIdentifier(sectionName, "array", packageName);
        TypedArray tarray = resources.obtainTypedArray(itemIdsArrayId);
        testIds = new int[tarray.length()];
        for (int i = 0; i < tarray.length(); i++) {
            testIds[i] = tarray.getResourceId(i, 0);
        }
        tarray.recycle();

        int itemTitlesArrayId = resources.getIdentifier(sectionName + "_titles", "array", packageName);
        testTitles = resources.getStringArray(itemTitlesArrayId);

        int itemDescriptionsArrayId = resources.getIdentifier(sectionName + "_descriptions", "array", packageName);
        testDescriptions = resources.getStringArray(itemDescriptionsArrayId);

        int itemHintsArrayId = resources.getIdentifier(sectionName + "_hints", "array", packageName);
        testHints = resources.getStringArray(itemHintsArrayId);

        mTests = new Test[testIds.length];
        for (int i = 0; i < testIds.length; i++) {
            Test test = new Test();
            test.setId(testIds[i]);
            test.setTitle(testTitles[i]);
            test.setDescription(testDescriptions[i]);
            test.setHint(testHints[i]);
            mTests[i] = test;
        }
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = new TestFragment();

        Bundle arguments = new Bundle();
        arguments.putSerializable(TestFragment.TEST_KEY, mTests[i]);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public int getCount() {
        return mTests == null ? 0 : mTests.length;
    }

    public Test getTest(int position) {
        return mTests[position];
    }
}
