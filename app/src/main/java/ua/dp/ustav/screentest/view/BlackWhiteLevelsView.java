package ua.dp.ustav.screentest.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import ua.dp.ustav.screentest.R;
import ua.dp.ustav.screentest.TestActivity;

/**
 * Helps to set brightness and contrast
 */
public class BlackWhiteLevelsView extends View {
    private static final int DELAY = 500;

    private final Paint paint = new Paint();
    private final TextPaint textPaint = new TextPaint();
    private final RectF rect = new RectF();

    private final int backgroundColor;
    private final int levelAddition;

    private boolean barsVisible = true;
    private long lastDraw;
    private boolean animate = false;

    public BlackWhiteLevelsView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.BlackWhiteLevelsView,
                0, 0);

        boolean black;
        try {
            black = typedArray.getBoolean(R.styleable.BlackWhiteLevelsView_black, true);
        } finally {
            typedArray.recycle();
        }
        backgroundColor = black ? Color.BLACK : Color.WHITE;
        levelAddition = black ? 0 : 225;

        textPaint.setColor(black ? Color.WHITE : Color.BLACK);
        textPaint.setAntiAlias(true);

        setBackgroundColor(backgroundColor);
        if (context instanceof TestActivity) {
            animate = true;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        long currTime = System.currentTimeMillis();
        if (animate && (currTime - lastDraw < DELAY)) {
            return;
        }
        lastDraw = currTime;

        int width = getWidth();
        int height = getHeight();

        float colWidth = (float) width / 30;
        float padding = colWidth / 10;
        float contentWidth = colWidth - padding * 2;

        textPaint.setTextSize(padding * 4);

        rect.left = padding;
        rect.top = padding;
        rect.right = rect.left + contentWidth;
        rect.bottom = height - padding;

        int level;

        for (int i = 0; i < 30; i++) {
            level = i + levelAddition;

            if (barsVisible) {
                paint.setColor(Color.rgb(level, level, level));
            } else {
                paint.setColor(backgroundColor);
            }

            canvas.drawRect(rect, paint);

            String text = String.valueOf(level);
            float textWidth = textPaint.measureText(text);
            float shift = (contentWidth - textWidth) / 2;
            canvas.drawText(text, rect.left + shift, padding + textPaint.getTextSize(), textPaint);

            rect.left = rect.right + 2 * padding;
            rect.right = rect.left + contentWidth;
        }

        if (animate) {
            barsVisible = !barsVisible;
            postInvalidateDelayed(DELAY);
        }
    }
}
