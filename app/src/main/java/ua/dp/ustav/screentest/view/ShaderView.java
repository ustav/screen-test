package ua.dp.ustav.screentest.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import ua.dp.ustav.screentest.R;

/**
 * Uses bitmap shader to fill its area
 */
public class ShaderView extends View {
    private final Paint paint = new Paint();

    public ShaderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ShaderView,
                0, 0);

        try {
            Drawable drawable = typedArray.getDrawable(R.styleable.ShaderView_shaderBitmap);
            if (drawable != null && drawable instanceof BitmapDrawable) {
                BitmapShader shader = new BitmapShader(
                        ((BitmapDrawable) drawable).getBitmap(),
                        Shader.TileMode.REPEAT,
                        Shader.TileMode.REPEAT
                );
                paint.setShader(shader);
            }
        } finally {
            typedArray.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
    }

}
