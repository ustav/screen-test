package ua.dp.ustav.screentest.adapter;

import ua.dp.ustav.screentest.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TestsAdapter extends BaseAdapter {
    private int[] testIds;
    private String[] testTitles;
    private LayoutInflater layoutInflater;
    private Context context;
    private String sectionName;
    private int itemHeight;

    public String getSectionName() {
        return sectionName;
    }

    public void setItems(String sectionName) {
        this.sectionName = sectionName;
        int itemIdsArrayId = context.getResources().getIdentifier(sectionName, "array", context.getPackageName());
        TypedArray tarray = context.getResources().obtainTypedArray(itemIdsArrayId);
        testIds = new int[tarray.length()];
        for (int i = 0; i < tarray.length(); i++) {
            testIds[i] = tarray.getResourceId(i, 0);
        }
        tarray.recycle();

        int itemTitlesArrayId = context.getResources().getIdentifier(sectionName + "_titles", "array", context.getPackageName());
        testTitles = context.getResources().getStringArray(itemTitlesArrayId);

        notifyDataSetChanged();
    }

    public TestsAdapter(Context context) {
        this.context = context;
        itemHeight = context.getResources().getDimensionPixelSize(R.dimen.grid_item_height);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return testIds == null ? 0 : testIds.length;
    }

    @Override
    public Object getItem(int position) {
        return testIds == null ? null : testIds[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TestHolder holder;

        if (convertView == null) {
            holder = new TestHolder();
            convertView = layoutInflater.inflate(R.layout.grid_item, parent, false);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.content = (LinearLayout) convertView.findViewById(R.id.content);
            convertView.setTag(holder);
            convertView.setLayoutParams(new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, itemHeight));
        } else {
            holder = (TestHolder) convertView.getTag();
        }

        View view = layoutInflater.inflate(testIds[position], null);
        view.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        holder.content.removeAllViews();
        holder.content.addView(view);
        holder.title.setText(testTitles[position]);
        return convertView;
    }

    private class TestHolder {
        TextView title;
        LinearLayout content;
    }
}
