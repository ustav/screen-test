package ua.dp.ustav.screentest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class TestFragment extends Fragment {
    public static final String TEST_KEY = "test";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Test test = (Test) getArguments().getSerializable(TEST_KEY);

        View view = inflater.inflate(R.layout.test, container, false);
        LinearLayout content = (LinearLayout) view.findViewById(R.id.content);
        View contentView = inflater.inflate(test.getId(), null);
        contentView.setLayoutParams(
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                )
        );
        content.addView(contentView);
        return view;
    }
}
