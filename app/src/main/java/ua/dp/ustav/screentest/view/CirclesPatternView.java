package ua.dp.ustav.screentest.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class CirclesPatternView extends View {
    private final Paint paint = new Paint();
    private final Paint crossPaint = new Paint();

    public CirclesPatternView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1);
        paint.setAntiAlias(true);

        crossPaint.setAntiAlias(false);
        crossPaint.setColor(Color.GREEN);
        crossPaint.setStrokeWidth(3);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        float smallRadius = height / 8;
        float bigRadius = height / 2;
        float cellSize = smallRadius / 2;

        for (int i = 0; i * cellSize < height; i++) {
            canvas.drawLine(0, i * cellSize, width, i * cellSize, paint);
        }

        int last = -1;
        for (int i = 0; i * cellSize < width / 2; i++) {
            canvas.drawLine(width / 2 + i * cellSize, 0, width / 2 + i * cellSize, height, paint);
            canvas.drawLine(width / 2 - i * cellSize, 0, width / 2 - i * cellSize, height, paint);
            last++;
        }
        canvas.drawLine(0, height - 1, width, height - 1, paint);

        float left = width / 2 - cellSize * last;
        float right = width / 2 + cellSize * last;

        canvas.drawCircle(left + smallRadius, smallRadius, smallRadius, paint);
        canvas.drawCircle(right - smallRadius, smallRadius, smallRadius, paint);
        canvas.drawCircle(right - smallRadius, height - smallRadius, smallRadius, paint);
        canvas.drawCircle(left + smallRadius, height - smallRadius, smallRadius, paint);
        canvas.drawCircle(width / 2, height / 2, bigRadius, paint);


        canvas.drawLine(left + smallRadius - cellSize / 2, smallRadius, left + smallRadius + cellSize / 2, smallRadius, crossPaint);
        canvas.drawLine(left + smallRadius - cellSize / 2, height - smallRadius, left + smallRadius + cellSize / 2, height - smallRadius, crossPaint);
        canvas.drawLine(right - smallRadius - cellSize / 2, smallRadius, right - smallRadius + cellSize / 2, smallRadius, crossPaint);
        canvas.drawLine(right - smallRadius - cellSize / 2, height - smallRadius, right - smallRadius + cellSize / 2, height - smallRadius, crossPaint);

        canvas.drawLine(left + smallRadius, smallRadius - cellSize / 2, left + smallRadius, smallRadius + cellSize / 2, crossPaint);
        canvas.drawLine(left + smallRadius, height - smallRadius - cellSize / 2, left + smallRadius, height - smallRadius + cellSize / 2, crossPaint);
        canvas.drawLine(right - smallRadius, smallRadius - cellSize / 2, right - smallRadius, smallRadius + cellSize / 2, crossPaint);
        canvas.drawLine(right - smallRadius, height - smallRadius - cellSize / 2, right - smallRadius, height - smallRadius + cellSize / 2, crossPaint);

        canvas.drawLine(width / 2 - cellSize / 2, height / 2, width / 2 + cellSize / 2, height / 2, crossPaint);
        canvas.drawLine(width / 2, height / 2 - cellSize / 2, width / 2, height / 2 + cellSize / 2, crossPaint);
    }
}
