package ua.dp.ustav.screentest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MenuFragment extends Fragment {
    private SectionChangeListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu, container, false);

        TextView resolution = (TextView) view.findViewById(R.id.resolution);

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        int width;
        int height;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Point size = new Point();
            display.getRealSize(size);
            width = size.x;
            height = size.y;
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            display.getSize(size);
            width = size.x;
            height = size.y;
        } else {
            width = display.getWidth();  // deprecated
            height = display.getHeight();  // deprecated
        }


//        int width = getResources().getDisplayMetrics().widthPixels;
//        int height = getResources().getDisplayMetrics().heightPixels;
        resolution.setText(getString(R.string.resolution) + " " + width + "x" + height);

        TextView density = (TextView) view.findViewById(R.id.density);
        int screenDensityRes;

        switch (getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                screenDensityRes = R.string.density_low;
                break;
            case DisplayMetrics.DENSITY_HIGH:
                screenDensityRes = R.string.density_high;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                screenDensityRes = R.string.density_xhigh;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                screenDensityRes = R.string.density_xxhigh;
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                screenDensityRes = R.string.density_xxxhigh;
                break;
            case DisplayMetrics.DENSITY_TV:
                screenDensityRes = R.string.density_tv;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
            default:
                screenDensityRes = R.string.density_medium;
                break;
        }

        density.setText(getString(R.string.density) + " " + getString(screenDensityRes) + " (" + getResources().getDisplayMetrics().densityDpi + " dpi)");

        String[] sectionTitles = getResources().getStringArray(R.array.section_titles);
        ListView sections = (ListView) view.findViewById(R.id.sections);
        sections.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.list_item, R.id.title, sectionTitles));
        sections.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listener != null) {
                    listener.onSectionChange(position);
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (SectionChangeListener) activity;
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

    public interface SectionChangeListener {
        void onSectionChange(int section);
    }
}
