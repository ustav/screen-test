package ua.dp.ustav.screentest;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MenuFragment.SectionChangeListener {

    private MenuFragment.SectionChangeListener listener;
    private String[] sectionTitles;
    private Fragment fragment;
    private View about;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        sectionTitles = getResources().getStringArray(R.array.section_titles);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            fragment = getSupportFragmentManager().findFragmentById(R.id.tests);
            if (fragment != null) {
                listener = (MenuFragment.SectionChangeListener) fragment;
                listener.onSectionChange(0);
            }
            about = findViewById(R.id.about);
        } else {
            listener = null;
        }

        TextView appVersion = (TextView) findViewById(R.id.app_version);
        if (appVersion != null) {
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                String version = pInfo.versionName;
                appVersion.setText(getString(R.string.version, version));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSectionChange(int section) {
        if (listener != null) {
            listener.onSectionChange(section);

            if (section == sectionTitles.length - 1) {
                if (about != null && about.getVisibility() == View.GONE) {
                    about.setVisibility(View.VISIBLE);
                }

                if (fragment != null && !fragment.isHidden()) {
                    getSupportFragmentManager().beginTransaction().hide(fragment).commit();
                }
            } else {
                if (about != null && about.getVisibility() == View.VISIBLE) {
                    about.setVisibility(View.GONE);
                }

                if (fragment != null && fragment.isHidden()) {
                    getSupportFragmentManager().beginTransaction().show(fragment).commit();
                }
            }
        } else {
            Intent intent;
            if (section == sectionTitles.length - 1) {
                intent = new Intent(this, AboutActivity.class);
            } else {
                intent = new Intent(this, TestsActivity.class);
            }
            intent.putExtra("section", section);
            startActivity(intent);
        }
    }
}