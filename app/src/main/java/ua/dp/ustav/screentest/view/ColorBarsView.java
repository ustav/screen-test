package ua.dp.ustav.screentest.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import ua.dp.ustav.screentest.TestActivity;

public class ColorBarsView extends View {
//	public static final int WHITE = Color.rgb(235, 235, 235);
//	public static final int BLACK = Color.rgb(16, 16, 16);
//	public static final int RED = Color.rgb(235, 16, 16);
//	public static final int GREEN = Color.rgb(16, 235, 16);
//	public static final int BLUE = Color.rgb(16, 16, 235);
//	public static final int CYAN = Color.rgb(16, 235, 235);
//	public static final int MAGENTA = Color.rgb(235, 16, 235);
//	public static final int YELLOW = Color.rgb(235, 235, 16);

    private static final int DELAY = 500;

    private static final int BRIGHT = 192;
    private static final int DARK = 13;

    private static final int WHITE = Color.rgb(BRIGHT, BRIGHT, BRIGHT);
    private static final int BLACK = Color.rgb(DARK, DARK, DARK);
    private static final int RED = Color.rgb(BRIGHT, DARK, DARK);
    private static final int GREEN = Color.rgb(DARK, BRIGHT, DARK);
    private static final int BLUE = Color.rgb(DARK, DARK, BRIGHT);
    private static final int CYAN = Color.rgb(DARK, BRIGHT, BRIGHT);
    private static final int MAGENTA = Color.rgb(BRIGHT, DARK, BRIGHT);
    private static final int YELLOW = Color.rgb(BRIGHT, BRIGHT, DARK);

    private static final int[] colorsBig = new int[]{WHITE, YELLOW, CYAN, GREEN, MAGENTA, RED, BLUE};
    private static final int[] colorsSmall = new int[]{BLUE, BLACK, MAGENTA, BLACK, CYAN, BLACK, WHITE};

    private final Paint paint = new Paint();
    private final TextPaint textPaint = new TextPaint();

    private boolean barsVisible = true;
    private long lastDraw;
    private boolean animate = false;

    public ColorBarsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (context instanceof TestActivity) {
            animate = true;
        }

        textPaint.setColor(Color.BLACK);
        textPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        long currTime = System.currentTimeMillis();
        if (animate && (currTime - lastDraw < DELAY)) {
            return;
        }
        lastDraw = currTime;

        int width = getWidth();
        int height = getHeight();

        textPaint.setTextSize(height / 30);

        float step = width / colorsBig.length;
        float margin = step / 5;
        float heightBig = height * 0.91f;
        float heightSmall = height * 0.1f;
        float heightMedium = heightBig * 0.2f;

        for (int i = 0; i < colorsBig.length; i++) {
            paint.setColor(colorsBig[i]);
            canvas.drawRect(i * step, 0, (i + 1) * step, heightBig, paint);

            paint.setColor(colorsSmall[i]);
            canvas.drawRect(i * step, height - heightSmall, (i + 1) * step, height, paint);

            String text = null;

            int colorSmall = colorsSmall[i];
            if (colorSmall == WHITE || colorSmall == BLUE) {
                text = "Color";
            } else if (colorSmall == CYAN || colorSmall == MAGENTA) {
                text = "Hue";
            }

            if (text != null) {
                float textWidth = textPaint.measureText(text);
                canvas.drawText(text, i * step + (step - textWidth) / 2, heightMedium * 2 - margin, textPaint);
            }

            if (barsVisible && colorsSmall[i] != BLACK) {
                canvas.drawRect(i * step + margin, heightMedium * 2, (i + 1) * step - margin, heightMedium * 3, paint);
            }
        }

        if (animate) {
            barsVisible = !barsVisible;
            postInvalidateDelayed(DELAY);
        }
    }

}
