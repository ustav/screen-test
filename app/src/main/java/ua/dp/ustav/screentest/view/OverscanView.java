package ua.dp.ustav.screentest.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

public class OverscanView extends View {

    private final int[] colors = new int[]{Color.GREEN, Color.CYAN, Color.YELLOW, Color.RED};
    private final int[] frames = new int[]{0, 2, 5, 10};

    private final Paint paint = new Paint();
    private final TextPaint textPaint;

    public OverscanView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);

        textPaint = new TextPaint();
        textPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
//		paint.setStrokeWidth(1);
        int width = getWidth();
        int height = getHeight();

//		for (int i=0; i<51; i+=10) {
//			canvas.drawRect(i, i, width-i-1, height-i-1, paint);
//			
//		}

        String text;
        float textWidth;

        textPaint.setTextSize(width / 100);

        for (int i = 0; i < colors.length; i++) {
            paint.setColor(colors[i]);
            textPaint.setColor(colors[i]);
            canvas.drawRect(frames[i] * width / 100, frames[i] * height / 100, width - frames[i] * width / 100, height - frames[i] * height / 100, paint);
            text = frames[i] + "%";
            textWidth = textPaint.measureText(text);
            canvas.drawText(text, (width - textWidth) / 2, textPaint.getTextSize() + frames[i] * height / 100, textPaint);
            canvas.drawText(text, (width - textWidth) / 2, height - frames[i] * height / 100 - 3, textPaint);
            canvas.drawText(text, frames[i] * width / 100 + 3, (height - textPaint.getTextSize()) / 2, textPaint);
            canvas.drawText(text, width - textWidth - frames[i] * width / 100 - 3, (height - textPaint.getTextSize()) / 2, textPaint);
        }


    }

}
