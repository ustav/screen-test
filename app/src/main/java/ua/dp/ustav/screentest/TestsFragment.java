package ua.dp.ustav.screentest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import ua.dp.ustav.screentest.adapter.TestsAdapter;

public class TestsFragment extends Fragment implements MenuFragment.SectionChangeListener {
    private TestsAdapter adapter;
    private String[] sectionNames;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        adapter = new TestsAdapter(getActivity());
        sectionNames = getResources().getStringArray(R.array.section_names);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tests, container, false);

        GridView grid = (GridView) view.findViewById(R.id.tests);
        grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), TestActivity.class);
                intent.putExtra(TestActivity.KEY_SECTION_NAME, adapter.getSectionName());
                intent.putExtra(TestActivity.KEY_ITEM_POSITION, position);
                startActivity(intent);
            }
        });

        if (getArguments() != null) {
            int section = getArguments().getInt("section", 0);
            setupSection(section);
        }

        return view;
    }

    private void setupSection(int section) {
        if (adapter != null) {
            adapter.setItems(sectionNames[section]);
        }
    }

    @Override
    public void onSectionChange(int section) {
        setupSection(section);
    }
}
