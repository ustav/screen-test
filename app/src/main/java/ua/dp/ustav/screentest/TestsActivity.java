package ua.dp.ustav.screentest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class TestsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tests_fragment);

        int section = getIntent().getIntExtra("section", 0);

        String[] sectionTites = getResources().getStringArray(R.array.section_titles);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setTitle(sectionTites[section]);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.tests);
        ((MenuFragment.SectionChangeListener) fragment).onSectionChange(section);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
