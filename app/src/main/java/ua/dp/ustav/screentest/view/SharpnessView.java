package ua.dp.ustav.screentest.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class SharpnessView extends View {
    private final Paint paint = new Paint();
    private final RectF oval = new RectF();

    public SharpnessView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundColor(Color.BLACK);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        paint.setColor(Color.WHITE);
        for (int k = 0; k < width; k += 10) {
            for (int l = 0; l < height; l += 10) {
                canvas.drawPoint(k, l, paint);
            }
        }

        canvas.save();
        canvas.translate((width - height) / 2, 0);
        canvas.clipRect(0, 0, height, height);

        paint.setColor(Color.GRAY);
        canvas.drawRect(0, 0, height, height, paint);


//		float thickness = (float)height/2000;
//		float drawnHeight1 = 1; 
//		while (drawnHeight1<height) {
//			paint.setColor(Color.BLACK);
//			canvas.drawRect(height/10+1, drawnHeight1, height/5, drawnHeight1+thickness, paint);
//			drawnHeight1+=thickness;
//			thickness*=1.01f;
//			Log.e(getClass().getSimpleName(), "thickness: " + thickness);
//			paint.setColor(Color.GRAY);
//			canvas.drawRect(height/10+1, drawnHeight1, height/5, drawnHeight1+thickness, paint);
//			drawnHeight1+=thickness;
//			thickness*=1.01f;
//			Log.e(getClass().getSimpleName(), "thickness: " + thickness);
//		}

        paint.setAntiAlias(true);
        int centerX = height / 10 + height / 10 + 5;
        int centerY = height / 10 + 5;

        paint.setColor(Color.WHITE);
        canvas.drawCircle(centerX, centerY, height / 10, paint);
        paint.setColor(Color.BLACK);
        for (int i = height / 10; i > 0; i -= 2) {
            paint.setColor(Color.BLACK);
            canvas.drawCircle(centerX, centerY, i, paint);
            paint.setColor(Color.WHITE);
            canvas.drawCircle(centerX, centerY, i - 1, paint);
        }

        centerX = height - height / 10 - 5;
        paint.setColor(Color.WHITE);
        canvas.drawCircle(centerX, centerY, height / 10, paint);
        paint.setColor(Color.BLACK);
        for (int i = height / 10; i > 0; i -= 4) {
            paint.setColor(Color.BLACK);
            canvas.drawCircle(centerX, centerY, i, paint);
            paint.setColor(Color.WHITE);
            canvas.drawCircle(centerX, centerY, i - 2, paint);
        }

        centerY = height - height / 5 - 5;
        paint.setColor(Color.WHITE);
        canvas.drawCircle(centerX, centerY, height / 10, paint);
        paint.setColor(Color.BLACK);
        for (int i = height / 10; i > 0; i -= 6) {
            paint.setColor(Color.BLACK);
            canvas.drawCircle(centerX, centerY, i, paint);
            paint.setColor(Color.WHITE);
            canvas.drawCircle(centerX, centerY, i - 3, paint);
        }

        centerX = height / 5 + 5;
        paint.setColor(Color.WHITE);
        canvas.drawCircle(centerX, centerY, height / 10, paint);
        paint.setColor(Color.BLACK);
        for (int i = height / 10; i > 0; i -= 8) {
            paint.setColor(Color.BLACK);
            canvas.drawCircle(centerX, centerY, i, paint);
            paint.setColor(Color.WHITE);
            canvas.drawCircle(centerX, centerY, i - 4, paint);
        }

        paint.setAntiAlias(false);

        int drawnHeight = 1;
        for (int i = 1; drawnHeight < height; i++) {
            paint.setColor(Color.BLACK);
            canvas.drawRect(1, drawnHeight, height / 10, drawnHeight + i, paint);
            drawnHeight += i;
            paint.setColor(Color.GRAY);
            canvas.drawRect(1, drawnHeight, height / 10, drawnHeight + i, paint);
            drawnHeight += i;
        }

        int drawnWidth = 1;
        for (int i = 1; drawnWidth < height; i++) {
            paint.setColor(Color.BLACK);
            canvas.drawRect(height - drawnWidth - i, height - height / 10, height - drawnWidth, height - 1, paint);
            drawnWidth += i;
            paint.setColor(Color.GRAY);
            canvas.drawRect(height - drawnWidth - i, height - height / 10, height - drawnWidth, height - 1, paint);
            drawnWidth += i;
            i++;
        }

        paint.setColor(Color.GRAY);
        canvas.drawRect(0, height - height / 10, height / 10, height, paint);
        paint.setColor(Color.BLACK);
        canvas.drawRect(1, height - height / 10, height / 10, height - 1, paint);

        canvas.save();
        canvas.translate(height / 3, height / 6);
        canvas.rotate(5.7f);
        paint.setAntiAlias(true);
        canvas.drawRect(0, 0, height / 2, height / 2, paint);
        paint.setAntiAlias(false);

        canvas.clipRect(0, 0, height / 2, height / 2);

        canvas.rotate(-5.7f);
        paint.setColor(Color.WHITE);
        for (int i = -height / 4; i < height / 2 + height / 4; i += 10) {
            for (int j = -height / 4; j < height / 2 + height / 4; j += 10) {
                canvas.drawPoint(i, j, paint);
            }
        }

        paint.setAntiAlias(true);
        canvas.rotate(5.7f);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(height / 4, height / 4, height / 5, paint);
        paint.setColor(Color.BLACK);
        float paddingcircle = (float) height / 4 - (float) height / 5 + 2;
        oval.set(paddingcircle, paddingcircle, height / 2 - paddingcircle, height / 2 - paddingcircle);

        for (int i = 0; i < 360; i += 2) {
            canvas.drawArc(oval, i, 1, true, paint);
        }
        paint.setAntiAlias(false);


        canvas.restore();

        canvas.restore();

        int padding = width / 50;

        canvas.save();
        canvas.translate(padding, padding);
        drawPattern(canvas, (width - height) / 2 - padding * 2);
        canvas.restore();

        canvas.save();
        canvas.translate(width + padding - (width - height) / 2, height + padding - (width - height) / 2);
        drawPattern(canvas, (width - height) / 2 - padding * 2);
        canvas.restore();

        canvas.save();
        canvas.translate(padding, height + padding - (width - height) / 2);
        drawVerticalLines(canvas, (width - height) / 2 - padding * 2);
        canvas.restore();

        canvas.save();
        canvas.translate(width + padding - (width - height) / 2, padding);
        drawHorizontalLines(canvas, (width - height) / 2 - padding * 2);
        canvas.restore();
    }

    private void drawVerticalLines(Canvas canvas, int size) {
        Paint paint = new Paint();
        paint.setColor(Color.DKGRAY);
        paint.setStrokeWidth(0);
        paint.setStrokeCap(Cap.SQUARE);
        paint.setStyle(Paint.Style.FILL);

        canvas.drawRect(0, 0, size, size, paint);

        int padding = size / 20;

        canvas.save();
        canvas.clipRect(0, 0, size, size);

        paint.setColor(Color.BLACK);
        int shift = 0;
        for (int i = 0; i * padding < size - padding - shift; i++) {
            canvas.drawRect(i * padding + shift, padding, i * padding + i + shift, size - padding, paint);
            shift += i + 1;
        }

        canvas.restore();
    }

    private void drawHorizontalLines(Canvas canvas, int size) {
        Paint paint = new Paint();
        paint.setColor(Color.DKGRAY);
        paint.setStrokeWidth(0);
        paint.setStrokeCap(Cap.SQUARE);
        paint.setStyle(Paint.Style.FILL);

        canvas.drawRect(0, 0, size, size, paint);

        int padding = size / 20;

        canvas.save();
        canvas.clipRect(0, 0, size, size);

        paint.setColor(Color.BLACK);
        int shift = 0;
        for (int i = 0; i * padding < size - padding - shift; i++) {
            canvas.drawRect(padding, i * padding + shift, size - padding, i * padding + i + shift, paint);
            shift += i + 1;
        }

        canvas.restore();
    }

    private void drawPattern(Canvas canvas, int size) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(0);
        paint.setStrokeCap(Cap.SQUARE);
        paint.setStyle(Paint.Style.FILL);

        size -= size % 4;

        int rectSize = size / 4;
        int strictRectSize = rectSize - rectSize % 4;

        canvas.save();
        canvas.clipRect(0, 0, size, size);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                canvas.save();
                canvas.translate(j * rectSize, i * rectSize);
                canvas.clipRect(0, 0, rectSize, rectSize);

                int current = i * 4 + j;
                switch (current) {
                    case 0:
                    case 15:
                        paint.setColor(Color.BLACK);
                        canvas.drawRect(0, 0, rectSize, rectSize, paint);
                        paint.setColor(Color.WHITE);
                        for (int k = 0; k < strictRectSize / 2; k += 2) {
                            canvas.drawLine(k, k, strictRectSize - k, k, paint);
                            canvas.drawLine(k, strictRectSize - k - 1, strictRectSize - k, strictRectSize - k - 1, paint);
                            canvas.drawLine(k, k, k, strictRectSize - k, paint);
                            canvas.drawLine(strictRectSize - k - 1, k, strictRectSize - k - 1, strictRectSize - k, paint);
                        }
                        break;
                    case 1:
                    case 14:
                        paint.setColor(Color.WHITE);
                        canvas.drawRect(0, 0, rectSize, rectSize, paint);
                        paint.setColor(Color.BLACK);
                        for (int k = 0; k < rectSize; k += 2) {
                            canvas.drawLine(0, k, rectSize, k, paint);
                        }
                        break;
                    case 2:
                    case 13:
                        paint.setColor(Color.WHITE);
                        canvas.drawRect(0, 0, rectSize, rectSize, paint);
                        paint.setColor(Color.BLACK);
                        for (int k = 0; k < rectSize; k += 2) {
                            canvas.drawLine(k, 0, k, rectSize, paint);
                        }
                        break;
                    case 3:
                    case 12:
                        paint.setColor(Color.BLACK);
                        canvas.drawRect(0, 0, rectSize, rectSize, paint);
                        paint.setColor(Color.WHITE);
                        for (int k = 0; k < strictRectSize / 2; k += 4) {
                            canvas.drawLine(k, k, strictRectSize - k, k, paint);
                            canvas.drawLine(k + 1, k + 1, strictRectSize - k - 1, k + 1, paint);

                            canvas.drawLine(k, strictRectSize - k - 1, strictRectSize - k, strictRectSize - k - 1, paint);
                            canvas.drawLine(k + 1, strictRectSize - k - 2, strictRectSize - k - 1, strictRectSize - k - 2, paint);

                            canvas.drawLine(k, k, k, strictRectSize - k, paint);
                            canvas.drawLine(k + 1, k + 1, k + 1, strictRectSize - k - 1, paint);

                            canvas.drawLine(strictRectSize - k - 1, k, strictRectSize - k - 1, strictRectSize - k, paint);
                            canvas.drawLine(strictRectSize - k - 2, k + 1, strictRectSize - k - 2, strictRectSize - k - 1, paint);
                        }
                        break;
                    case 4:
                    case 11:
                        paint.setColor(Color.WHITE);
                        canvas.drawRect(0, 0, rectSize, rectSize, paint);
                        paint.setColor(Color.BLACK);
                        for (int k = 0; k < rectSize; k += 4) {
                            canvas.drawLine(k, 0, k, rectSize, paint);
                            canvas.drawLine(k + 1, 0, k + 1, rectSize, paint);
                        }
                        break;
                    case 5:
                    case 10:
                        paint.setColor(Color.BLACK);
                        canvas.drawRect(0, 0, rectSize, rectSize, paint);
                        paint.setColor(Color.WHITE);
                        for (int k = 0; k < rectSize; k++) {
                            int start = k % 2;
                            for (int l = start; l < rectSize; l += 2) {
                                canvas.drawPoint(k, l, paint);
                            }
                        }
                        break;
                    case 6:
                        paint.setColor(Color.WHITE);
                        canvas.drawRect(0, 0, rectSize, rectSize, paint);
                        paint.setColor(Color.BLACK);
                        for (int k = 0; k < rectSize; k += 4) {
                            for (int l = 0; l < rectSize; l += 4) {
                                canvas.drawRect(k, l, k + 2, l + 2, paint);
                            }
                        }
                        break;
                    case 7:
                    case 8:
                        paint.setColor(Color.WHITE);
                        canvas.drawRect(0, 0, rectSize, rectSize, paint);
                        paint.setColor(Color.BLACK);
                        for (int k = 0; k < rectSize; k += 4) {
                            canvas.drawLine(0, k, rectSize, k, paint);
                            canvas.drawLine(0, k + 1, rectSize, k + 1, paint);
                        }
                        break;
                    case 9:
                        paint.setColor(Color.WHITE);
                        canvas.drawRect(0, 0, rectSize, rectSize, paint);
                        paint.setColor(Color.BLACK);
                        for (int k = 0; k < rectSize; k += 2) {
                            for (int l = 0; l < rectSize; l += 2) {
                                canvas.drawPoint(k, l, paint);
                            }
                        }
                        break;
                }

                canvas.restore();
            }

        }

        canvas.restore();
    }

}
